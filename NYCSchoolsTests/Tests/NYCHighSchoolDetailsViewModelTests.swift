//
//  NYCHighSchoolDetailsViewModelTests.swift
//  NYCSchoolsTests
//
//  Created by Karthik Padala on 6/26/22.
//

import XCTest
@testable import NYCSchools

class NYCHighSchoolDetailsViewModelTests: XCTestCase {
    var viewModel: NYCHighSchoolDetailsViewModel!
    
    override func setUp() {
        super.setUp()
    }
    override func tearDown() {
        super.tearDown()
    }
    
    func testToVerifyIntializer() {
        let schoolData = SchoolData(dbn: "02M260", school_name: "Clinton School Writers & Artists, M.S. 260", building_code: "M868")
        let satScore = SATScores(dbn: "01M292", school_name: "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES", num_of_sat_test_takers: "29", sat_critical_reading_avg_score: "355", sat_math_avg_score: "404", sat_writing_avg_score: "363")
        
        viewModel = NYCHighSchoolDetailsViewModel(schoolData: schoolData, satScore: satScore)
        
        XCTAssertNotNil(viewModel.schoolData)
        XCTAssertNotNil(viewModel.satScore)
        
        XCTAssert(viewModel.schoolData?.dbn == "02M260")
        XCTAssert(viewModel.schoolData?.school_name == "Clinton School Writers & Artists, M.S. 260")
        XCTAssert(viewModel.schoolData?.building_code == "M868")
        
        XCTAssert(viewModel.satScore?.dbn == "01M292")
        XCTAssert(viewModel.satScore?.school_name == "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
        XCTAssert(viewModel.satScore?.num_of_sat_test_takers == "29")
        XCTAssert(viewModel.satScore?.sat_critical_reading_avg_score == "355")
        XCTAssert(viewModel.satScore?.sat_math_avg_score == "404")
        XCTAssert(viewModel.satScore?.sat_writing_avg_score == "363")
    }
}
