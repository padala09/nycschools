//
//  NetworkManagerTests.swift
//  NYCSchoolsTests
//
//  Created by Karthik Padala on 6/25/22.
//

import XCTest
@testable import NYCSchools

class NetworkManagerTests: XCTestCase {
    var networkManager: NetworkManager!
    
    var session = MockURLSession()
    
    override func setUp() {
        super.setUp()
        networkManager = NetworkManager(session: session)
    }
    override func tearDown() {
        super.tearDown()
    }
    
    func testDataRequestwithURL() {
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else {
            fatalError("URL can't be empty")
        }
        networkManager.dataRequest(with: url, objectType: [SchoolData].self) { data, response, err in
            
        }
        XCTAssert(session.lastURL == url)
    }
    
    func test_get_resume_called() {
        let dataTask = MockURLSessionDataTask()
        session.nextDataTask = dataTask
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else {
            fatalError("URL can't be empty")
        }
        networkManager.dataRequest(with: url, objectType: [SchoolData].self) { data, response, err in
            
        }
        XCTAssert(dataTask.resumeWasCalled)
    }
}
