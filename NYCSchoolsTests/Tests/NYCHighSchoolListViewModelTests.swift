//
//  NYCHighSchoolListViewModelTests.swift
//  NYCSchoolsTests
//
//  Created by Karthik Padala on 6/25/22.
//

import XCTest
@testable import NYCSchools

class NYCHighSchoolListViewModelTests: XCTestCase {
    let session = MockURLSession()
    var viewModel: NYCHighSchoolListViewModel!
    
    override func setUp() {
        super.setUp()
        viewModel = NYCHighSchoolListViewModel(session: session)
    }
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetSchoolsList() {
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else {
            fatalError("URL can't be empty")
        }
        viewModel.getSchoolsList(url: url) { responseSuccess, err in
            
        }
        XCTAssert(session.lastURL == url)
    }
    
    func testGetSchoolsSATScores() {
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json") else {
            fatalError("URL can't be empty")
        }
        viewModel.getSchoolsSATScores(url: url)
        XCTAssert(session.lastURL == url)
    }
    
    func testResumeWasCalled() {
        let dataTask = MockURLSessionDataTask()
        session.nextDataTask = dataTask
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else {
            fatalError("URL can't be empty")
        }
        viewModel.getSchoolsList(url: url) { responseSuccess, err in
            
        }
        XCTAssert(dataTask.resumeWasCalled)
    }
}
