//
//  MockURLSessionDataTask.swift
//  NYCSchoolsTests
//
//  Created by Karthik Padala on 6/26/22.
//

import Foundation
@testable import NYCSchools

/// In URLSession, the return value must be a URLSessionDataTask. However, the URLSessionDataTask can’t be created programmatically, thus, this is an object that needs to be mocked
/// As URLSessionDataTask, this mock has the same method, resume(). So it might be able to treat this mock as the return value of the dataTask()
class MockURLSessionDataTask: URLSessionDataTaskProtocol {
    private (set) var resumeWasCalled = false
    func resume() {
        resumeWasCalled = true
    }
}
