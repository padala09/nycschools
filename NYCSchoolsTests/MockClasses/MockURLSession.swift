//
//  MockURLSession.swift
//  NYCSchoolsTests
//
//  Created by Karthik Padala on 6/26/22.
//

import Foundation
@testable import NYCSchools

/// This MockURLSession acts like a URLSession. Both of URLSession and MockURLSession have the same method, dataTask(), and the same callback closure type.
class MockURLSession: URLSessionProtocol {
    func dataTask(with request: URLRequest, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol {
        lastURL = request.url
        completionHandler(nil, nil, nil)
        return nextDataTask
    }
    
    var nextDataTask = MockURLSessionDataTask()
    private (set) var lastURL: URL?
}
