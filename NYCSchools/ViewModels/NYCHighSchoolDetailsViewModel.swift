//
//  NYCHighSchoolDetailsViewModel.swift
//  NYC High Schools
//
//  Created by Karthik Padala on 6/25/22.
//

import Foundation

class NYCHighSchoolDetailsViewModel {
    /// Property to store selected school data
    var schoolData: SchoolData?
    /// Property to store selected school SAT scores
    var satScore: SATScores?
    
    /// Intializer
    init(schoolData: SchoolData?, satScore: SATScores?) {
        self.schoolData = schoolData
        self.satScore = satScore
    }
}
