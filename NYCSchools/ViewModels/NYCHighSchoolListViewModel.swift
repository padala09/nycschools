//
//  NYCSchoolListViewModel.swift
//  NYC High Schools
//
//  Created by Karthik Padala on 6/25/22.
//

import Foundation

class NYCHighSchoolListViewModel {
    /// Property to store the downloaded school data
    var schoolsData: [SchoolData]?
    /// Property to store the downloaded schools SAT Scores
    var satScores: [SATScores]?
    /// Property to set the session it can be `URLSession` or `MockSession` for unit testing which is of `URLSessionProtocol` type
    var session: URLSessionProtocol
    
    /// Intializer to set session
    init(session: URLSessionProtocol) {
        self.session = session
    }
    
    /// Func to download the list of schools and returned to view after completion
    func getSchoolsList(url: URL, completionHandler: @escaping (Bool?, Error?) -> Void) {
        NetworkManager(session: self.session).dataRequest(with: url, objectType: [SchoolData].self) { [weak self] data, response, err in
            guard let data = data else {
                completionHandler(nil, err)
                return
            }
            self?.schoolsData = data
            completionHandler(true, nil)
        }
    }
    
    /// Function to download schools SAT scores
    func getSchoolsSATScores(url: URL) {
        NetworkManager(session: self.session).dataRequest(with: url, objectType: [SATScores].self) { [weak self] scores, response, err in
            if let satScores = scores {
                self?.satScores = satScores
            }
        }
    }
}

