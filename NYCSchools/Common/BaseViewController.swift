//
//  UIViewController+Extension.swift
//  NYCSchools
//
//  Created by Karthik Padala on 1/12/22.
//

import UIKit

/// ViewController to define re-usable `UIViewController` functions
class BaseViewController: UIViewController {
    /// Activity Indicator
    private let activityIndicator = UIActivityIndicatorView(style: .large)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpActivityIndicator()
    }
    
    /// Add constraint to Activity indicator
    private func setUpActivityIndicator() {
        /// add indicator as subview to main view
        view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.color = .darkGray
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor)
        ])
    }
    
    /// Starts Acitiviy Indicator Animation
    func startAnimation() {
        activityIndicator.startAnimating()
    }
    
    /// stops Acitiviy Indicator Animation
    func stopAnimation() {
        activityIndicator.stopAnimating()
    }
    
    func showAlertView(_ title: String, _ message: String, completion: @escaping () -> ()) {
        // create the alert
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: completion)
    }
}
