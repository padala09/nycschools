//
//  NYCHighSchoolCommonUtilities.swift
//  NYC High Schools
//
//  Created by Karthik Padala on 1/11/22.
//

import Foundation
import UIKit

class NYCHighSchoolCommonUtilities {
   static func defaultColor() -> UIColor {
      return UIColor(red: 39/255, green: 92/255, blue: 178/255, alpha: 1.0)
    }
}
