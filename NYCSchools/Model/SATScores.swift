//
//  SATScores.swift
//  NYC High Schools
//
//  Created by Karthik Padala on 6/25/22.
//

import Foundation

/// Model to represent the SAT scores
struct SATScores: Codable {
    var dbn: String? = nil
    var school_name: String? = nil
    var num_of_sat_test_takers: String? = nil
    var sat_critical_reading_avg_score: String? = nil
    var sat_math_avg_score: String? = nil
    var sat_writing_avg_score: String? = nil
    
    init(dbn: String? = nil, school_name: String? = nil, num_of_sat_test_takers: String? = nil, sat_critical_reading_avg_score: String? = nil, sat_math_avg_score: String? = nil, sat_writing_avg_score: String? = nil) {
        self.dbn = dbn
        self.school_name = school_name
        self.num_of_sat_test_takers = num_of_sat_test_takers
        self.sat_critical_reading_avg_score = sat_critical_reading_avg_score
        self.sat_math_avg_score = sat_math_avg_score
        self.sat_writing_avg_score = sat_writing_avg_score
    }
}
