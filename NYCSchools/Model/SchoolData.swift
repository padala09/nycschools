//
//  SchoolData.swift
//  NYC High Schools
//
//  Created by Karthik Padala on 6/25/22.
//

import Foundation

/// Model to represent Schools List
public struct SchoolData: Codable {
    var dbn: String? = nil
    var school_name: String? = nil
    var overview_paragraph: String? = nil
    var building_code: String? = nil
    var location: String? = nil
    var phone_number: String? = nil
    var fax_number: String? = nil
    var school_email: String? = nil
    var website: String? = nil
    var primary_address_line_1: String? = nil
    var borough: String? = nil
    
    init(dbn: String? = nil, school_name: String? = nil, overview_paragraph: String? = nil, building_code: String? = nil, location: String? = nil, phone_number: String? = nil, fax_number: String? = nil, school_email: String? = nil, website: String? = nil, primary_address_line_1: String? = nil, borough: String? = nil) {
        self.dbn = dbn
        self.school_name = school_name
        self.overview_paragraph = overview_paragraph
        self.building_code = building_code
        self.location = location
        self.phone_number = phone_number
        self.fax_number = fax_number
        self.school_email = school_email
        self.website = website
        self.primary_address_line_1 = primary_address_line_1
        self.borough = borough
    }
}
