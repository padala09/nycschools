//
//  URLSessionExtensions.swift
//  NYCSchools
//
//  Created by Karthik Padala on 6/26/22.
//

import Foundation

/// **URLSession Extension**
/// The session must be either `URLSession` or `MockURLSession`. So we change the type from URLSession to a protocol, URLSessionProtocol
/// Now we are able to inject either URLSession or MockURLSession or whatever object that conforms this protocol.
protocol URLSessionProtocol { typealias DataTaskResult = (Data?, URLResponse?, Error?) -> Void
    func dataTask(with request: URLRequest, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol
}

/// URLSession does not have a dataTask() returning URLSessionDataTaskProtocol. So we need to extend a method to conform the protocol.
/// This is a simple method converting the return type from URLSessionDataTask to URLSessionDataTaskProtocol. It won’t change the behavior of the dataTask() at all
extension URLSession: URLSessionProtocol {
    func dataTask(with request: URLRequest, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol {
        return (dataTask(with: request, completionHandler: completionHandler) as URLSessionDataTask) as URLSessionDataTaskProtocol
    }
}

/// **URLSessionDataTask Extension**
/// To make the return type of same kind we create another protocol which has same func of `URLSessionDataTask`
/// The URLSessionDataTask has the exact same protocol method, resume(), so nothing happens about URLSessionDataTask.
protocol URLSessionDataTaskProtocol { func resume() }

/// `URLSessionDataTask` conforming to `URLSessionDataTaskProtocol`
extension URLSessionDataTask: URLSessionDataTaskProtocol {}



