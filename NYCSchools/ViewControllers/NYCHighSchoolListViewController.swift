//
//  NYCHighSchoolListViewController.swift
//  NYC High Schools
//
//  Created by Karthik Padala on 6/25/22.
//

import Foundation
import UIKit

class NYCHighSchoolListViewController: BaseViewController {
    /// viewModel to fetch required data to build the view
    var viewModel: NYCHighSchoolListViewModel?
    /// Table View to display school list
    @IBOutlet weak var schoolListTableView: UITableView!
    
    /// TableView Refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
                                    #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.gray
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = NYCHighSchoolListViewModel(session: URLSession.shared)
        setUpNavigationBar()
        configureTableView()
        buildSchoolsData()
        prepareSchoolsSATScores()
    }
    
    /// Set required configuration to tableView
    private func configureTableView() {
        /// Conform to tableView delegates
        schoolListTableView.delegate = self
        schoolListTableView.dataSource = self
        
        self.schoolListTableView.addSubview(self.refreshControl)
        
        /// Set row height for tableView
        schoolListTableView.rowHeight = UITableView.automaticDimension
        schoolListTableView.estimatedRowHeight = UITableView.automaticDimension
        /// Register tableView cell
        schoolListTableView.register(UINib(nibName: "NYCHighSchoolsListTableViewCell", bundle: nil), forCellReuseIdentifier: Constants.schoolListCellIdentifier)
    }
    
    /// This function gets the list of schools calling the service.
    /// If the response is success, it reloads the tableView and will display the list of schools
    /// If service returns any error, it navigates user to the error view
    private func buildSchoolsData()  {
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else { return }
        startAnimation()
        viewModel?.getSchoolsList(url: url, completionHandler: { [weak self] responseSuccess, err in
            /// Response Success Scenario
            if responseSuccess ?? false {
                self?.schoolListTableView.reloadData()
                self?.stopAnimation()
                self?.refreshControl.endRefreshing()
                return
            }
            /// If service returns any error scenario
            if let err = err {
                self?.stopAnimation()
                self?.showAlertView("Service Error", err.localizedDescription, completion: {
                    self?.refreshControl.endRefreshing()
                })
            }
        })
    }
    
    /// configure navigation bar
    private func setUpNavigationBar() {
        navigationItem.title = Constants.schoolsListViewTitle
    }
    
    /// Downloads the SAT Scores of schools and persists the data in viewModel
    private func prepareSchoolsSATScores()  {
        guard let url = URL(string: Constants.SATScoresURL) else { return }
        viewModel?.getSchoolsSATScores(url: url)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        buildSchoolsData()
        prepareSchoolsSATScores()
    }
}
    
/// Implementation of tableView delegate methods
extension NYCHighSchoolListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.schoolListCellIdentifier, for: indexPath) as? NYCHighSchoolsListTableViewCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        guard let schoolsData = viewModel?.schoolsData else { return cell }
        /// Get school data for respective row while loading the tableview
        let school = schoolsData[indexPath.row]
        cell.schoolName.text = school.school_name
        cell.schoolCode.text = "\(Constants.schoolCode) \(school.dbn ?? "")"
        cell.schoolAddress.text = "\(school.primary_address_line_1 ?? ""), \(school.borough ?? "")"
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.schoolsData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /// get school data for selected row
        let selectedSchool = viewModel?.schoolsData?[indexPath.row]
        /// Get SAT score for selected school
        let selectedSchoolSATScores = viewModel?.satScores?.first { $0.dbn == selectedSchool?.dbn } ?? nil
        
        /// Build detail screen Viewmodel
        let schoolDetailViewModel = NYCHighSchoolDetailsViewModel(schoolData: selectedSchool, satScore: selectedSchoolSATScores)
        /// Navigate to the detail screen with selected school data and SAT Score
        self.navigationController?.pushViewController(NYCHighSchoolDetailsViewController(viewModel: schoolDetailViewModel), animated: true)
    }
}
