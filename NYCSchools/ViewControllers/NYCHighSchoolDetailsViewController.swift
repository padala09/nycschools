//
//  NYCHighSchoolDetailsViewController.swift
//  NYC High Schools
//
//  Created by Karthik Padala on 6/25/22.
//

import Foundation
import UIKit

/// ViewController which displays the selected school details and SAT Scores
class NYCHighSchoolDetailsViewController: UIViewController {
    /// viewModel to fetch the required data to build the view
    var viewModel: NYCHighSchoolDetailsViewModel?
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    /// outlets to set school details
    @IBOutlet weak var schoolNameAndCode: UILabel!
    @IBOutlet weak var schoolOverview: UILabel!
    @IBOutlet weak var overviewHeader: UILabel!
    
    /// outlets to set school SAT scores
    @IBOutlet weak var satScoresHeading: UILabel!
    @IBOutlet weak var numberOfTestTakers: UILabel!
    @IBOutlet weak var readingTestScore: UILabel!
    @IBOutlet weak var mathAverageTestScore: UILabel!
    @IBOutlet weak var writingTestScore: UILabel!
    
    /// outlets to set school address
    @IBOutlet weak var addressHeading: UILabel!
    @IBOutlet weak var schoolAddressAndContactInfo: UILabel!
    
    /// View Intializer
    convenience init(viewModel: NYCHighSchoolDetailsViewModel) {
        self.init()
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = Constants.schoolSATScoreViewTitle
        configureViewLabels()
        buildScreen()
    }
    
    /// Set the text color to view labels
    private func configureViewLabels() {
        overviewHeader.text = Constants.overViewDescription
        satScoresHeading.text = Constants.SATScoresHeading
        addressHeading.text = Constants.schoolAddressHeader
        schoolNameAndCode.textColor = NYCHighSchoolCommonUtilities.defaultColor()
        satScoresHeading.textColor = NYCHighSchoolCommonUtilities.defaultColor()
        addressHeading.textColor = NYCHighSchoolCommonUtilities.defaultColor()
        overviewHeader.textColor = NYCHighSchoolCommonUtilities.defaultColor()
    }
    
    /// Set data to UI Elements
    private func buildScreen() {
        guard let schoolData = viewModel?.schoolData else { return }
        let satScore = viewModel?.satScore
        
        /// Set School Details
        schoolNameAndCode.text = "\n \(schoolData.school_name ?? "") \n \(Constants.schoolCode) \(schoolData.dbn ?? "") \n"
        schoolOverview.text = "\(schoolData.overview_paragraph ?? "")\n\n"
        
        /// Set SAT Scores
        /// If SAT Scores are `nil`, change the header text
        if satScore == nil {
            satScoresHeading.text = Constants.schoolSATScoresNotAvailable
        }
        numberOfTestTakers.text = "\(Constants.numberOfTestTakers)  \(satScore?.num_of_sat_test_takers ?? "---")"
        readingTestScore.text = "\(Constants.readingScore)  \(satScore?.sat_critical_reading_avg_score ?? "---")"
        mathAverageTestScore.text = "\(Constants.mathAverageScore)  \(satScore?.sat_math_avg_score ?? "---")"
        writingTestScore.text = "\(Constants.writingScore)  \(satScore?.sat_writing_avg_score ?? "---")\n\n"
        
        /// Set School Contact Info
        schoolAddressAndContactInfo.text = "\(Constants.address)\n\n \(schoolData.location ?? "---") \n \(Constants.phoneNumber) \(schoolData.phone_number ?? "---") \n \(Constants.faxNumber) \(schoolData.fax_number ?? "---") \n  \(Constants.email) \(schoolData.school_email ?? "---") \n \(Constants.website) \(schoolData.website ?? "---")\n"
    }
}

