//
//  NYCHighSchoolLandingViewController.swift
//  NYC High Schools
//
//  Created by Karthik Padala on 6/25/22.
//

import Foundation
import UIKit

class NYCHighSchoolLandingViewController: UIViewController {
    /// UI Outlets
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var schoolListButton: UIButton!
    @IBOutlet weak var schoolImage: UIImageView!
            
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    /// Set data to the UI Labels
    func configureUI() {
        navigationItem.title = Constants.landingViewTitle
        schoolListButton.setTitle(Constants.landingViewBtnTitle, for: .normal)

        headerLabel.text = Constants.laandingViewDescription
        headerLabel.textColor = NYCHighSchoolCommonUtilities.defaultColor()
        
        schoolListButton.backgroundColor = NYCHighSchoolCommonUtilities.defaultColor()
        schoolListButton.layer.cornerRadius = 10
        schoolImage.layer.cornerRadius = 20
    }
    
    /// Button Action to navigate to School list viewController
    @IBAction func schoolListButtonAction(_ sender: Any) {
        self.navigationController?.pushViewController(NYCHighSchoolListViewController(), animated: true)
    }
}
