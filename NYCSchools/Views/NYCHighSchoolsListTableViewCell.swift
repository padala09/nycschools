//
//  NYCHighSchoolListTableViewCell.swift
//  NYC High Schools
//
//  Created by Karthik Padala on 6/25/22.
//

import Foundation
import UIKit

class NYCHighSchoolsListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var schoolCode: UILabel!
    @IBOutlet weak var schoolAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
