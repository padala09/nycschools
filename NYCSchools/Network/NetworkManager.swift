//
//  NetworkManager.swift
//  NYC High Schools
//
//  Created by Karthik Padala on 6/25/22.
//

import Foundation

/// Class which manages all the network calls
public class NetworkManager {
    
    /// Property to manage the session
    private let session: URLSessionProtocol
    
    /// Initializer to set the session it can be `URLSession` or `MockSession` for unit testing which is of `URLSessionProtocol` type
    init(session: URLSessionProtocol) {
        self.session = session
    }
    
    /// Func which sends request to given URL and convert to Decodable Object
    /// - Parameters:
    ///    - url: Remote URL to send request to service
    ///    - objectType: Type of object to which the downloaded data needs to be decoded
    ///    - completionHandler: called when download task is completed to give back data to caller
    func dataRequest<T: Decodable>(with url: URL,
                                   objectType: T.Type,
                                   completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) {
        /// create the URLRequest object
        let request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60)
        
        /// create dataTask using the session object, which uses `MockSession` for unit testing and `NSURLSession` for real case.
        let task = session.dataTask(with: request, completionHandler: { data, response, error in
            /// Update the UI on main thread
            DispatchQueue.main.async {
                guard let data = data else { return }
                do {
                    /// create decodable object from data and call completion handler with the result
                    let decodedObject = try JSONDecoder().decode(objectType.self, from: data)
                    completionHandler(decodedObject, response, error)
                } catch let error {
                    completionHandler(nil, response, error)
                }
            }
        })
        task.resume()
    }
}



