//
//  Constants.swift
//  NYC High Schools
//
//  Created by Karthik Padala on 6/25/22.
//

import Foundation

struct Constants {
    /// Network URL's
    static let schoolListURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    static let SATScoresURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    
    ///  LandingView Constants
    static let landingViewTitle = "NYC HIGH SCHOOLS"
    static let landingViewBtnTitle = "Click here for NYC High Schools list"
    static let laandingViewDescription = "Welcome to NYC High Schools"
    
    /// School List view constants
    static let schoolsListViewTitle = "NYC High Schools list"
    static let schoolSATScoreViewTitle = "School SAT Scores"
    static let schoolSATScoresNotAvailable = "Selected School SAT Scores are not available"
    static let schoolListCellIdentifier = "schoolListCell"
    
    ///  School Details view constants
    static let numberOfTestTakers = "Number of SAT Test Takers:"
    static let readingScore = "SAT Reading Test Score:"
    static let mathAverageScore = "SAT Math Test Score:"
    static let writingScore = "SAT Writing Test Score:"
    static let schoolCode = "School Code:"
    static let address = "Address:"
    static let phoneNumber = "Phone Number:"
    static let faxNumber = "fax Number:"
    static let email = "Email:"
    static let website = "Website:"
    static let overViewDescription = "About School:"
    static let SATScoresHeading = "School SAT Scores:"
    static let schoolAddressHeader = "School Contact Info:"
}
